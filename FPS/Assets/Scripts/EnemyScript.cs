using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class EnemyScript : MonoBehaviour
{
    public Transform player;
    public NavMeshAgent navMesh;
    public EnemyStats enemyStats;
    bool detectedEnemy = false;

    private void Awake()
    {
        navMesh = GetComponentInParent<NavMeshAgent>();
        StartCoroutine(EnemyDirection());
    }
    void Start()
    {

    }


    void Update()
    {
        if (Vector3.Distance(player.position, transform.position) < enemyStats.DetectDistance)
        {
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out RaycastHit hitInfo, Mathf.Infinity))
            {
                print("Tocando " + hitInfo.transform.name);
            }
            navMesh.SetDestination(player.position);
            navMesh.speed = 5;
        }
    }

    IEnumerator EnemyDirection()
    {
        while (true)
        {
            yield return new WaitForSeconds(5f);
            print("a");
            transform.rotation = Quaternion.Lerp(transform.rotation, transform.rotation * transform.rotation, 1000);

        }

    }


}
