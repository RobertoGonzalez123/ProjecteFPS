using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip AK47, AK47R, AA12, AA12R;
    static AudioSource audioSrc;
    // Start is called before the first frame update
    void Start()
    {
        AK47 = Resources.Load<AudioClip>("AK47");
        AK47R = Resources.Load<AudioClip>("AK47R");
        AA12 = Resources.Load<AudioClip>("AA12");
        AA12R = Resources.Load<AudioClip>("AA12R");

        audioSrc = GetComponent<AudioSource>();
    }

    public static void PlaySound(string sound)
    {
        switch (sound)
        {
            case "AK47":
                audioSrc.PlayOneShot(AK47);
                break;
            case "AK47R":
                audioSrc.PlayOneShot(AK47R);
                break;
            case "AA12":
                audioSrc.PlayOneShot(AA12);
                break;
            case "AA12R":
                audioSrc.PlayOneShot(AA12R);
                break;
        }
    }
}
