using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingSystem : MonoBehaviour
{
    [Header("RECOIL")]
    [SerializeField]
    Transform dondemira;



    [Header("HUD References")]
    public TMPro.TextMeshProUGUI MagazineNumber;
    public TMPro.TextMeshProUGUI ExtraNumber;

    [Header("Particles")]
    public ParticleSystem MuzzleFlash;
    public ParticleSystem HitSparkle;

    [Header("Raycast")]
    //Where PlayerCameraIsLookingAt
    [SerializeField]
    private Transform PlayerLookingTo;

    //What bullet hits
    RaycastHit WhatIsShooted;

    [Header("Weapons")]
    [SerializeField]
    private WeaponInfo weapon;

    Animator m_animator;

    [SerializeField]
    private WeaponInfo weapon2;


    //PrimaryWeaponAmmo
    int MagazineAmmoLeft;
    int ExtraAmmoLeft;
    int bulletsToShotThisBurst;


    // ShootingStates
    bool m_Shooting = false;
    bool m_Reloading = false;
    bool m_CanShoot =true;

    private void Awake()
    {
        MagazineAmmoLeft = weapon.magazineSize;
        ExtraAmmoLeft = weapon.extraAmmo;
        MagazineNumber.text = "" + MagazineAmmoLeft;
        ExtraNumber.text = "" + ExtraAmmoLeft;
        m_animator = GetComponentInChildren<Animator>();
    }
    private void Update()
    {
        UpdateStates();
    }

    private void UpdateStates()
    {
        // UPDATE SHOOTING BOOLEAN
        // AUTO - if weapon is auto it'll just check if mouse button is pressed
        // SEMI - if weapon is not auto it'll check if player tapped it
        if (weapon.IsWeaponAuto)
            m_Shooting = Input.GetKey(KeyCode.Mouse0);
        else
            m_Shooting = Input.GetKeyDown(KeyCode.Mouse0);


        // call ToReload() function if magazine isn't full, player isn't reloading and has at least 1 extra bullet remaining
        if (Input.GetKeyDown(KeyCode.R) && MagazineAmmoLeft < weapon.magazineSize && !m_Reloading && ExtraAmmoLeft>0)
            ToReload();

        // call ToShoot() function if player isn't reloading, magazine>1bullet, canShoot and is trying to shoot
        if (!m_Reloading && MagazineAmmoLeft > 0 && m_CanShoot && m_Shooting)
        {
            bulletsToShotThisBurst = weapon.bulletsFiredByShot;
            ToShoot();
        }
            
    }
    private void ToReload()
    {
        m_Reloading = true;
        StartCoroutine(ReloadCoroutine());
    }

    private IEnumerator ReloadCoroutine()
    {
        SoundManager.PlaySound(weapon.weaponSound+"R");
        if (MagazineAmmoLeft == 0)
            m_animator.Play("RELOAD0");
        else
            m_animator.Play("RELOAD");



        yield return new WaitForSeconds(weapon.reloadTime);
        // if you need more or the exact quantity of ammo left, just reload all the remaining left and set it to 0
        if ((weapon.magazineSize - MagazineAmmoLeft) >= ExtraAmmoLeft)
        {
            MagazineAmmoLeft += ExtraAmmoLeft;
            ExtraAmmoLeft = 0;
        }
        else // si tienes mas de la que necesitas para llenar el cargador, lo haces y restas la recargada de la extra
        {
            ExtraAmmoLeft -= (weapon.magazineSize - MagazineAmmoLeft);
            MagazineAmmoLeft = weapon.magazineSize;
        }
        MagazineNumber.text = "" +MagazineAmmoLeft;
        ExtraNumber.text = "" + ExtraAmmoLeft;
        m_Reloading = false;
    }

    private void CanShootAgain()
    {
        m_CanShoot = true;
    }
    private void ToShoot()
    {
        m_CanShoot = false;
        MuzzleFlash.Play();
        m_animator.Play("FIRE");
        SoundManager.PlaySound(weapon.weaponSound);

        
        if (weapon.pelletWeapon)
            for(int i=0;i< weapon.pellets; i++)
            {
                float recoilH = Random.Range(-weapon.horizontalRecoil, weapon.horizontalRecoil);
                float recoilV = Random.Range(-weapon.verticalRecoil, weapon.verticalRecoil);
                if (Physics.Raycast(PlayerLookingTo.position, PlayerLookingTo.forward + new Vector3(recoilH, recoilV, 0), out WhatIsShooted, 50))
                    Instantiate(HitSparkle, WhatIsShooted.point, Quaternion.LookRotation(WhatIsShooted.normal));
            }
        else
        {
            float recoilH = Random.Range(-weapon.horizontalRecoil, weapon.horizontalRecoil);
            float recoilV = Random.Range(-weapon.verticalRecoil, weapon.verticalRecoil);
            if (Physics.Raycast(PlayerLookingTo.position, PlayerLookingTo.forward + new Vector3(recoilH, recoilV, 0), out WhatIsShooted, 50))
                Instantiate(HitSparkle, WhatIsShooted.point, Quaternion.LookRotation(WhatIsShooted.normal));
        }

        this.GetComponentInParent<RECOIL>().addRecoil();
        MagazineAmmoLeft--;
        bulletsToShotThisBurst--;
        MagazineNumber.text = "" + MagazineAmmoLeft;
        if (bulletsToShotThisBurst > 0 && MagazineAmmoLeft > 0)
            Invoke("ToShoot", weapon.timeBetweenBullets); 
        else
            Invoke("CanShootAgain", weapon.timeBetweenShootAction);

    }
    

    
}
