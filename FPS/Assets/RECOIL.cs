using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RECOIL : MonoBehaviour
{
    Vector3 actual;
    Vector3 adonde;

    [SerializeField]
    float inclinacion=0.2f;
    [SerializeField]
    float horizontal = 1;
    [SerializeField]
    float vertical = 4.7f;

    [SerializeField]
    float lerp, slerp = 5;

    private void Update()
    {
        updatePosition();
    }
    private void updatePosition()
    {
        adonde = Vector3.Lerp(adonde, Vector3.zero, Time.fixedDeltaTime * lerp);//r
        actual = Vector3.Slerp(actual, adonde, Time.fixedDeltaTime * slerp);//s
        transform.localRotation = Quaternion.Euler(actual);
    }
    public void addRecoil()
    {
        Debug.Log("me llamaron");
        float a = Random.Range(-horizontal, horizontal);
        float b = Random.Range(-inclinacion, inclinacion);

        adonde += new Vector3(-vertical,a, b);    
        
    }

    
    
}
